package edu.utexas.tacc.portlet.tests;

import java.util.LinkedHashMap;

import org.ektorp.ComplexKey;
import org.ektorp.ViewResult;
import org.ektorp.ViewResult.Row;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;

import edu.utexas.tacc.portlet.service.CouchService;

public class CouchTest {
	
	private static String PROJECT_DATE_VIEW = "_design/project_date";
	private static String PROJECT_RESOURCE_DATE_VIEW = "_design/project_resource_date";
	private static String PROJECT_RESOURCE_USER_DATE_VIEW = "_design/project_resource_user_date";
	
	@Test
	public void testCouchQuery() {
		CouchService couch = new CouchService();
		
		if (couch == null) {
			System.out.println("Couch is null");
		}
		
		System.out.println("Running couch query test");
		
		ComplexKey startKey = ComplexKey.of("TG-ART11001", "2011-10"),
				endKey = ComplexKey.of("TG-ART11001", "2011-10");
		
		
		ViewResult projectJobsResult = couch.executeCouchQuery(PROJECT_DATE_VIEW, "jobs", startKey, endKey);
		ViewResult projectSusResult = couch.executeCouchQuery(PROJECT_DATE_VIEW, "sus", startKey, endKey);

		assert(!projectJobsResult.isEmpty());
		assert(!projectSusResult.isEmpty());
	}
	
	@Test
	public void testKeyHash() {
		System.out.println("Running key hash test");
		CouchService couch = new CouchService(); 
		
		ComplexKey startKey = ComplexKey.of("TG-ART11001", "2011-10"),
				endKey = ComplexKey.of("TG-ART11001", "2011-10");
		
		ViewResult projectJobsResult = couch.executeCouchQuery(PROJECT_DATE_VIEW, "jobs", startKey, endKey);
		
		for (Row row : projectJobsResult.getRows()) {
			LinkedHashMap<String, String> keys = couch.populateKeyHash(row.getKeyAsNode(), PROJECT_DATE_VIEW);
			
			assert(keys.size() == 2);
		}
	}
}
