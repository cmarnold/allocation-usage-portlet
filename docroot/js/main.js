(function($) {
	var type = "";
	$('.toggle').click(function(e) {
		e.preventDefault();
		type = $(this).attr('type');
		$('.inactive.'+type).toggle();
		$('.toggle.'+type).toggle();
	});
})(jQuery);