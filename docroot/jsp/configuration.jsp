<%@ include file="/jsp/init.jsp" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<form action="<liferay-portlet:actionURL portletConfiguration="true" />" method="post" name="<portlet:namespace />fm">
Select View Mode:  <br />

	<input type="radio" name="mode" value="full" ${mode eq 'full' ? 'checked' : ''} />&nbsp;&nbsp;Full
	<br /> 
	<input type="radio" name="mode" value="mini" ${mode eq 'mini' ? 'checked' : ''} />&nbsp;&nbsp;Mini
<br/><br />

<input type="button" value="Save" onClick="submitForm(document.<portlet:namespace />fm);" /> </form>