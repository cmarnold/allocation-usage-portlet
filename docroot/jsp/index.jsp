<%@ include file="/jsp/init.jsp"%>

<div class="allocation-usage-portlet">
	<liferay-ui:success key="success-make-allocation-mgr"
		message="success-make-allocation-mgr" />

	<h4 style="float: left;">Projects</h4>
	<div style="float: right;">
		<ul class="links">
			<li><a href="#" id="toggle-projects">Show Inactive Projects</a></li>
			<li><a href="#" id="toggle-allocs">Show Expired/Inactive
					Allocations</a></li>
		</ul>
	</div>

	<div class="divider"></div>
	<c:forEach var="project" items="${projects}">
		<div class="${project.active ? 'active' : 'inactive'}">
			<div class="project-header">
				<h6 class="project-header-text">
					${project.title}
					<!-- Probably do my nifty "roles" trick here rather than this -->
					<c:if test="${isAdmin || isAllocMgr || isPi || isCoPi}">
						<portlet:renderURL var="projectUsageUrl">
							<portlet:param name="action" value="project_usage" />
							<portlet:param name="project" value="${project.accountId}" />
						</portlet:renderURL>
						<a class="allocation-usage" href="${projectUsageUrl}"> <img
							class="tooltip usage"
							title="View usage for ${project.chargeNumber}"
							alt="View usage for ${project.chargeNumber}"
							src="${renderRequest.contextPath}/images/usage_32.png" />
						</a>
					</c:if>
				</h6>
				<ul>
					<li><strong>Project PI:</strong> <span>${project.pi.firstName}
							${project.pi.lastName}</span></li>
					<li><strong>Charge No.:</strong> <span>${project.chargeNumber}</span></li>
				</ul>
			</div>

			<div class="divider"></div>
			<table width="100%">
				<thead>
					<tr>
						<th>Resource</th>
						<th>Award</th>
						<th>Usage</th>
						<th>Burn Rate</th>
						<th>End Date<br />[Days Left]
						</th>
						<th>Type</th>
						<th>State</th>
						<th>Users</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${project.allocations}" var="alloc">
						<tr
							class="${alloc.resource.isActive && alloc.daysLeft gt 0 && alloc.remainingAlloc gt 0 ? 'active' : 'inactive'}">
							<td>${alloc.resource.commonName}</td>
							<td>
								<div class="tooltip"
									title="Award: ${alloc.baseAllocFormatted} GBs 
									<br />
									Remaining: ${alloc.remainingAllocFormatted} SUs
									<br/>My Usage: ${alloc.usedAllocFormatted} GBs">${alloc.percentRemain}
									| ${alloc.remainingAllocFormatted} GBs left <br /> <a
										class="load"> <span class="load-overlay"></span> <span
										class="load-value" style="width:${alloc.percentRemain};"></span>
									</a> from ${alloc.baseAllocFormatted} GB award
								</div>
							</td>
							<td class="usage"><portlet:renderURL var="allocUsageUrl">
									<portlet:param name="action" value="alloc_usage" />
									<portlet:param name="project" value="${project.chargeNumber}" />
									<portlet:param name="allocation" value="${alloc.allocationId}" />
								</portlet:renderURL> <a class="allocation-usage" href="${projectUsageUrl}"> <img
									class="tooltip"
									title="View detailed usage for ${alloc.resource.commonName}"
									alt="View detailed usage for ${alloc.resource.commonName}"
									src="${renderRequest.contextPath}/images/usage_32.png" />
							</a><br /> ${alloc.usedAllocFormatted} SUs</td>
							<td>
								<div class="tooltip"
									title="
									Ideal Burn Rate: ${alloc.idealBurnRateFormatted}
									(Current burn rate is ${alloc.burnRateRatioFormatted}% of ideal)">
									${alloc.burnRateFormatted}</div>
							</td>
							<td>
								<div class="tooltip"
									title="Start Date: <joda:format value="${alloc.initialStartDate}" pattern="MM/dd/yyyy" />">
									<joda:format value="${alloc.endDate}" pattern="MM/dd/yyyy" />
									[${alloc.daysLeft}d]
								</div>
							</td>
							<td>${alloc.allocationType}</td>
							<td>${alloc.resource.isActive && alloc.daysLeft gt 0 && alloc.remainingAlloc gt 0 ? 'active' : 'inactive'}</td>
							<td><portlet:resourceURL var="showUserUrl">
									<portlet:param name="resource" value="users" />
									<portlet:param name="project" value="${project.chargeNumber}" />
									<portlet:param name="alloc" value="${alloc.allocationId}" />
								</portlet:resourceURL> <a class="allocation-usage" href="${showUserUrl}"> <img
									class="tooltip usage"
									title="Manage users on ${alloc.resource.commonName}"
									alt="Manage users on ${alloc.resource.commonName}"
									src="${renderRequest.contextPath}/images/user_32.png" />
							</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="divider"></div>
		</div>
	</c:forEach>
	<fieldset class="admin">
		<legend>Admin</legend>
		<div class="fieldset-wrapper">
			<liferay-ui:error key="admin-username-override-dne"
				message="admin-username-override-dne" />
			<portlet:actionURL var="overrideUrl">
				<portlet:param name="action" value="adminOverride" />
				<portlet:param name="model" value="projects" />
			</portlet:actionURL>
			<form action="${overrideUrl}" method="post">
				<label for="adminOverrideUser">Username override</label> <input
					type="text" name="adminOverrideUser" value="${adminOverride}" /> <input
					type="hidden" name="redirect" value="<portlet:renderURL />" /> <input
					type="submit" value="Submit" />
			</form>
			<form action="${overrideUrl}" method="post">
				<input type="hidden" name="adminOverrideUser" value="" /> <input
					type="hidden" name="redirect" value="<portlet:renderURL />" /> <input
					type="submit" value="Reset" />
			</form>
		</div>
	</fieldset>
	<div>
		<p>Allocations for Supplements, Advances and Transfers appear in
			the portal before they appear in the accounting records at XSEDE
			sites. Please allow 24 hours after you receive an award notification
			for the allocation updates to appear in the accounting records of the
			XSEDE sites.</p>
	</div>
</div>
