package edu.utexas.tacc.portlet.service;

import javax.naming.NamingException;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.sql.DataSource;

import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import edu.utexas.tacc.bean.PortalPerson;
import edu.utexas.tacc.portlets.AbstractPortlet;

@Service(value="abstractPortletService")
public class AbstractPortletService extends AbstractPortlet {

	public PortalPerson getPerson(PortletRequest request) {

		PortalPerson person = null;
		
		try {
			person = getCurrentPerson(request);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return person;
	}

	public DataSource getDataSource() {
		
		DataSource ds = null;
		
		try {
			ds = getPortalDataSource();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ds;
	}

}
