package edu.utexas.tacc.portlet.service;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.annotation.PostConstruct;

import org.ektorp.ComplexKey;
import org.ektorp.ViewResult;
import org.ektorp.ViewResult.Row;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import edu.utexas.tacc.bean.PortalPerson;
import edu.utexas.tacc.portlet.model.Usage;

public class UsageManager {

	@Autowired
	public CouchService couch;
	
	private static String PROJECT_DATE_VIEW = "_design/project_date";
	private static String PROJECT_RESOURCE_DATE_VIEW = "_design/project_resource_date";
	private static String PROJECT_RESOURCE_USER_DATE_VIEW = "_design/project_resource_user_date";
	
	// Project level
	public LinkedList<Usage> getUsage(String chargeNumber, DateTime startDate, DateTime endDate) {
		LinkedList<Usage> usage = new LinkedList<Usage>();
		
		ComplexKey startKey = ComplexKey.of(chargeNumber, startDate.toString()),
				endKey = ComplexKey.of(chargeNumber, endDate.toString());
		
		ViewResult projectJobsResult = couch.executeCouchQuery(PROJECT_DATE_VIEW, "jobs", startKey, endKey);
		ViewResult projectSusResult = couch.executeCouchQuery(PROJECT_DATE_VIEW, "sus", startKey, endKey);
		
		return usage;
	}
	
	// Allocation level
	public LinkedList<Usage> getUsage(String chargeNumber, DateTime startDate, DateTime endDate, String resource) {
		LinkedList<Usage> usage = new LinkedList<Usage>();
		
		ComplexKey startKey = ComplexKey.of(chargeNumber, resource, startDate.toString()),
			endKey = ComplexKey.of(chargeNumber, resource, endDate.toString());
		
		ViewResult allocationJobsResult = couch.executeCouchQuery(PROJECT_RESOURCE_DATE_VIEW, "jobs", startKey, endKey);
		ViewResult allocationSusResult = couch.executeCouchQuery(PROJECT_RESOURCE_DATE_VIEW, "sus", startKey, endKey);
		
		return usage;
	}
	
	// Allocation level by user
	public LinkedList<Usage> getUsage(String chargeNumber, DateTime startDate, DateTime endDate, String resource, String username) {
		LinkedList<Usage> usage = new LinkedList<Usage>();
		
		ComplexKey startKey = ComplexKey.of(chargeNumber, resource, username, startDate.toString()),
				endKey = ComplexKey.of(chargeNumber, resource, username, endDate.toString());
			
		ViewResult allocationUserJobsResult = couch.executeCouchQuery(PROJECT_RESOURCE_USER_DATE_VIEW, "jobs", startKey, endKey);
		ViewResult allocationUserSusResult = couch.executeCouchQuery(PROJECT_RESOURCE_USER_DATE_VIEW, "sus", startKey, endKey);
		
		LinkedHashMap<String, Integer> jobs = new LinkedHashMap<String, Integer>();
		LinkedHashMap<String, Double> sus = new LinkedHashMap<String, Double>();
		
		for (Row row : allocationUserJobsResult.getRows()) {
			// Oh ho ho! Smell that simplicity.
			ComplexKey singleKey = ComplexKey.of(chargeNumber, resource, username,
					couch.populateKeyHash(row.getKeyAsNode(), PROJECT_RESOURCE_USER_DATE_VIEW).get("date"));
			
			jobs.putAll(getJobs(couch.executeCouchQuery(PROJECT_RESOURCE_USER_DATE_VIEW, "jobs", singleKey)));
		}
		
		for (Row row : allocationUserSusResult.getRows()) {
			ComplexKey singleKey = ComplexKey.of(chargeNumber, resource, username,
					couch.populateKeyHash(row.getKeyAsNode(), PROJECT_RESOURCE_USER_DATE_VIEW).get("date"));
			
			sus.putAll(getSus(couch.executeCouchQuery(PROJECT_RESOURCE_USER_DATE_VIEW, "sus", singleKey)));	
		}
		
			
		return usage;
	}
	
	private LinkedHashMap<String, Integer> getJobs(ViewResult result) {
		LinkedHashMap<String, Integer> jobs = new LinkedHashMap<String, Integer>();
		
		for (Row row : result.getRows()) {
			//String dateTimeValue = row.getDocAsNode().findValue("start_time").asText();
			//LocalDate date = new LocalDate(dateTimeValue.substring(0, dateTimeValue.indexOf(" ")));
			DateTime date = new DateTime(row.getDocAsNode().findValue("start_time").asText());
			Integer currCount = jobs.get(date.toString("MM/dd/yyyy"));
			int count = currCount == null ? 1 : currCount + 1;
			jobs.put(date.toString(), count);
		}
		
		return jobs;
	}
	
	private LinkedHashMap<String, Double> getSus(ViewResult result) {
		LinkedHashMap<String, Double> sus = new LinkedHashMap<String, Double>();
		
		for (Row row : result.getRows()) {
			// TODO : I am sure that joda has a better way to do this because seriously.
			//String dateTimeValue = row.getDocAsNode().findValue("start_time").asText();
			//LocalDate date = new LocalDate(dateTimeValue.substring(0, dateTimeValue.indexOf(" ")));
			DateTime date = new DateTime(row.getDocAsNode().findValue("start_time").asText());
			Double sum = row.getDocAsNode().findValue("sus").asDouble();
			Double prevSum = sus.get(date.toString("MM/dd/yyyy"));
			
			sum = prevSum == null ? Double.parseDouble(row.getValue()) : prevSum + Double.parseDouble(row.getValue());
			sus.put(date.toString(), sum);
		}
		
		return sus;
	}
	
}
