package edu.utexas.tacc.portlet.service;

import java.util.LinkedHashMap;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.ektorp.ComplexKey;
import org.ektorp.ViewQuery;
import org.ektorp.ViewResult;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import edu.utexas.tacc.clients.CouchClient;

@Service(value="couchService")
public class CouchService {
	
	private static final Logger logger = Logger.getLogger(CouchService.class);
	
	private CouchClient couchClient;
	
	@PostConstruct
	public void init() {		
		ResourceBundle bundle = ResourceBundle.getBundle("couchdb");
		String server = bundle.getString("server");
		int port = Integer.parseInt(bundle.getString("port"));
		boolean ssl = Boolean.parseBoolean(bundle.getString("ssl"));
		String database = bundle.getString("database");
		String couchuser = bundle.getString("username");
		String password = bundle.getString("password");
		couchClient = new CouchClient(server, port, database, couchuser, password, ssl);		
	}
	
	// query on a lot of records
	public ViewResult executeCouchQuery(String designDocument, String view, ComplexKey startKey, ComplexKey endKey) {
		ViewQuery query = couchClient.getViewQuery(designDocument, view, startKey, endKey, true, false).group(true);
		
		return couchClient.executeQuery(query);
	}	
	
	// query on a more targeted record
	public ViewResult executeCouchQuery(String designDocument, String view, ComplexKey singleKey) {
		ViewQuery query = couchClient.getViewQuery(designDocument, view, singleKey, false, true);
		
		return couchClient.executeQuery(query);
	}
	
	public LinkedHashMap<String, String> populateKeyHash(JsonNode node, String designDocument) {
		ResourceBundle bundle = ResourceBundle.getBundle("couchKeys");
		String keys = bundle.getString(designDocument);
		
		LinkedHashMap<String, String> keyHash = new LinkedHashMap<String, String>();

		int index = 0;
		for (String key : keys.split(",")) {
			keyHash.put(key, node.get(index).asText());
			index++;
		}
		
		return keyHash;
	}
}
