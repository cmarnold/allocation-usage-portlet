package edu.utexas.tacc.portlet.service;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.utexas.tacc.bean.Allocation;
import edu.utexas.tacc.bean.Project;
import edu.utexas.tacc.dao.AllocationDao;
import edu.utexas.tacc.dao.ProjectDao;
import edu.utexas.tacc.exceptions.QueryException;


@Service(value="allocationService")
public class AllocationService {
	
	private static final Logger logger = Logger.getLogger(AllocationService.class);

	public List<Project> getProjectsForUser(int personId, DataSource ds) {
		ProjectDao pdao = new ProjectDao(ds);
		AllocationDao adao = new AllocationDao(ds);

		List<Project> projects = new ArrayList<Project>();
		try {
			projects = pdao.getPersonProjects(personId, false);
			
			for (Project project : projects) {
				List<Allocation> allocations = adao.getAllocationsForProject(project.getAccountId());

				project.setAllocations(allocations);
			}
		} catch (QueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return projects;
	}
	

	public List<Project> getActiveProjectsForUser(int personId, DataSource ds) {
		ProjectDao pdao = new ProjectDao(ds);
		AllocationDao adao = new AllocationDao(ds);

		List<Project> projects = new ArrayList<Project>();
		try {			
			for (Project project : pdao.getPersonProjects(personId, false)) {
				if (project.isActive()) {
					List<Allocation> allocations = adao.getActiveAllocationsForProject(project.getAccountId());

					project.setAllocations(allocations);
					projects.add(project);
				}
			}
		} catch (QueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return projects;
	}

	public Project getProject(int accountId, DataSource ds) throws QueryException {
		ProjectDao pdao = new ProjectDao(ds);
		AllocationDao adao = new AllocationDao(ds);
		
		return pdao.getProjectByAccountId(accountId).setAllocations(adao.getAllocationsForProject(accountId));
	}

}
