package edu.utexas.tacc.portlet.model;

import org.joda.time.LocalDate;

public class Usage {
	private LocalDate date;
	private int jobs;
	private int sus;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public int getJobs() {
		return jobs;
	}
	public void setJobs(int jobs) {
		this.jobs = jobs;
	}
	public int getSus() {
		return sus;
	}
	public void setSus(int sus) {
		this.sus = sus;
	}
}
