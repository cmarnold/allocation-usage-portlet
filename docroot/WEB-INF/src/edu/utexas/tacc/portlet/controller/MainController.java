package edu.utexas.tacc.portlet.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.github.jknack.handlebars.helper.JodaHelper;
import com.github.jknack.handlebars.springmvc.HandlebarsViewResolver;

import edu.utexas.tacc.bean.PortalPerson;
import edu.utexas.tacc.bean.Project;
import edu.utexas.tacc.dao.PortalPersonDao;
import edu.utexas.tacc.portlet.handlebars.helpers.PortletUrlHelper;
import edu.utexas.tacc.portlet.service.AbstractPortletService;
import edu.utexas.tacc.portlet.service.AllocationService;


@Controller("mainController")
@RequestMapping(value="VIEW")
public class MainController {

	private static Logger logger = Logger.getLogger(MainController.class);

	@Autowired
	public AllocationService allocationService;

	@Autowired
	public AbstractPortletService abstractPortletService;

	@Autowired
	HandlebarsViewResolver viewResolver;
	
	@PostConstruct
	public void init() {		
		viewResolver.getHandlebars().registerHelper("renderURL", PortletUrlHelper.renderURL);
		viewResolver.getHandlebars().registerHelper("resourceURL", PortletUrlHelper.resourceURL);
		viewResolver.getHandlebars().registerHelper("actionURL", PortletUrlHelper.actionURL);
		viewResolver.getHandlebars().registerHelper("jodaPatternHelper", JodaHelper.jodaPattern);
	}


	@RenderMapping
	public String index(RenderRequest request) {
		PortletPreferences prefs = request.getPreferences();
		String view = prefs.getValue("mode", "full");
		
		return view.equals("full") ? "index" : "mini";
	}

	@ModelAttribute("projects")
	public List<Project> getProjects(PortletRequest request) {
		DataSource ds = abstractPortletService.getDataSource();
		PortalPerson person = abstractPortletService.getPerson(request);
		PortalPersonDao ppdao = new PortalPersonDao(ds);
		
		String adminOverride = (String)request.getPortletSession().getAttribute("adminOverride");
		if (adminOverride != null && !adminOverride.equals("")) {
			person = ppdao.get(adminOverride);
		}
		
		return allocationService.getProjectsForUser(person.getId(), ds);		
	}

	@ModelAttribute("activeProjects")
	public List<Project> getActiveProjects(PortletRequest request) {
		DataSource ds = abstractPortletService.getDataSource();
		PortalPerson person = abstractPortletService.getPerson(request);
		PortalPersonDao ppdao = new PortalPersonDao(ds);
		
		String adminOverride = (String)request.getPortletSession().getAttribute("adminOverride");
		if (adminOverride != null && !adminOverride.equals("")) {
			person = ppdao.get(adminOverride);
		}
		
		return allocationService.getActiveProjectsForUser(person.getId(), ds);		
	}

	@ModelAttribute("request")
	public PortletRequest getRequest(PortletRequest request) {
		return request;
	} 

	
	@ActionMapping(params={"action=adminOverride"})
	public void adminOverride(
			@RequestParam(value="adminOverrideUser", required=false) String adminOverride,
			@RequestParam(value="redirect", required=false) String redirect,
			ActionRequest request, ActionResponse response) throws IOException {
		
		if (adminOverride == null || adminOverride.equals("")) {
			adminOverride = abstractPortletService.getPerson(request).getUsername();
		}
		
		request.getPortletSession().setAttribute("adminOverride", adminOverride);
		response.sendRedirect(redirect);
	}
}
