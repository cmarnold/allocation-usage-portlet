package edu.utexas.tacc.portlet.controller;

import javax.annotation.PostConstruct;
import javax.portlet.PortletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.github.jknack.handlebars.helper.JodaHelper;
import com.github.jknack.handlebars.springmvc.HandlebarsViewResolver;

import edu.utexas.tacc.bean.Project;
import edu.utexas.tacc.exceptions.QueryException;
import edu.utexas.tacc.portlet.handlebars.helpers.PortletUrlHelper;
import edu.utexas.tacc.portlet.model.Usage;
import edu.utexas.tacc.portlet.service.AbstractPortletService;
import edu.utexas.tacc.portlet.service.AllocationService;
import edu.utexas.tacc.portlet.service.UsageManager;

@Controller("usageController")
@RequestMapping(value="VIEW")
public class UsageController {
	private static Logger logger = Logger.getLogger(UsageController.class);

	@Autowired
	public AllocationService allocationService;

	@Autowired
	public AbstractPortletService abstractPortletService;
	
	@Autowired
	public UsageManager usageManager;

	@Autowired
	HandlebarsViewResolver viewResolver;
	
	@PostConstruct
	public void init() {		
		viewResolver.getHandlebars().registerHelper("renderURL", PortletUrlHelper.renderURL);
		viewResolver.getHandlebars().registerHelper("resourceURL", PortletUrlHelper.resourceURL);
		viewResolver.getHandlebars().registerHelper("actionURL", PortletUrlHelper.actionURL);
		viewResolver.getHandlebars().registerHelper("jodaPatternHelper", JodaHelper.jodaPattern);
	}
	
	@RenderMapping(params={"view=usage"})
	public String usageView() {
		return "usage";
	}

	@ModelAttribute("usage")
	public Usage getUsage(@RequestParam("accountId") int accountId, PortletRequest request) {
		DataSource ds = abstractPortletService.getDataSource();
		Project project = null;
		Usage usage = null;
		try {
			project = allocationService.getProject(accountId, ds);
		} catch (QueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usage;
	}

	@ModelAttribute("request")
	public PortletRequest getRequest(PortletRequest request) {
		return request;
	} 

}
