package edu.utexas.tacc.portlet.handlebars.springmvc;

import com.github.jknack.handlebars.springmvc.HandlebarsView;

/** Copied from Matt's tup system monitor portlet **/
public class HandlebarsPortletView extends HandlebarsView {
	@Override
	protected boolean isContextRequired() {
		return false;
	}
}
