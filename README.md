# Spring MVC Portlet Template

This is a portlet template for creating Spring MVC portlets
within the Liferay Plugins SDK.

## Usage

Clone this repository into the Liferay Plugins SDK `tools`
directory.  Then you can use this template with the usual
portlet create script:

    $ cd /path/to/liferay-plugins-sdk/portlets
    $ ./create.sh awesome "My Awesome Portlet" springmvc

This will create `awesome-portlet` with all of the configuration
in place for Spring MVC.

